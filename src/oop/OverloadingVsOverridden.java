package oop;

public class OverloadingVsOverridden {

    // Overloading : the process of defining more than one method using the same method name
    //              but different parameter list
    // add(int a){}
    // add(double a) {}
    // add(int a, int b) {}

    // Overriding : the process of re-implementing of an inherited method
    //  using the same  method signature (method name + parameter list)
    // In override : the return data type can be changed only from super type tp sub type.

    static class Parent {
        //Overloading
       private void add() {return  ; }
       public Parent add(Parent p) {return  null; }
       public int add(int a) {return a;}
       public int add(int a, int b) {return a+b ;}
       protected void add(double b) {return  ; }
       void add(char b) {return  ; }

    }

   static  class Child extends Parent {
        // Overriding
       @Override
        public int add(int t) {
            return super.add(t);
        }
       @Override
       public Child add(Parent p) {
           return (Child) super.add(p);
       }

       @Override
       public int add(int a, int b) {
           return super.add(a, b);
       }

       @Override
       public void add(double b) {
           super.add(b);
       }

       @Override
       void add(char b) {
           super.add(b);
       }
   }
    public static void main(String[] args) {

        Child c = new Child();


    }
}
