package oop;

import static java.lang.System.out;

public class StaticDemo {

    // static keyword can be used with
    // 1) instance variables  :
    // 2) inner classes
    // 3) blocks  {}
    // 4) import
    // 5) methods
    {
        // this is a block
    }

    static  {
        // this is a static block
    }
    public  static  final String TAG ="This is the Tag";
    static  int StaticVariable ; //(also called class variables )
    int instanceVariable ;
    public  void method()
    {
        // local variable must be initialized before using it
         int localVariable =20 ;

        // System is a class
        // out is a variable

        System.out.println("localVariable = " + localVariable);

        // after adding import  static java.lang.System.out; we can use
         out.println("localVariable = " + localVariable);

        // instance variable must NOT be initialized before using it
        // because the compiler will give it a default value
        System.out.println("instanceVariable = " + instanceVariable);;
    }

    public static void main(String[] args) {
        StaticDemo.StaticVariable = 0 ;
    }
}
