package oop;

import shared.Person;


public class ObjectClassMethods {

    /**
     *
     * @param a
     * @param s
     * @param c
     * @return
     */
    public String MyMethod(int a, String s, int c)
    {
        return  a + s ;
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "ObjectClassMethods";
    }

    public static void main(String[] args) {

        ObjectClassMethods objectClassMethods = new ObjectClassMethods();
        objectClassMethods.MyMethod(5,"SDK",5);
        Object object = new Object();

        Object o = new Person();

        System.out.println(objectClassMethods.toString());
        System.out.println(objectClassMethods);

        String str = objectClassMethods.toString() ;

        System.out.println("object.hashCode() = " + object.hashCode());
        System.out.println("object.hashCode() = " + Integer.toHexString(object.hashCode()));
        System.out.println("object = " + object);

        Point p = new Point();
        p.x = 1;
        p.y = 2 ;
        p.z = 3 ;

        Point p1 = new Point();
        p1.x = 1;
        p1.y = 2 ;
        p1.z = 3 ;


       Point p2 = p1 ;     // create a new variable that referred to the object referred by p1
//        Point p2 = new Point() ;
//        p2.x =p1.x ;
//        p2.y = p1.y ;
//        p2.z = p1.z ;
        //Point p2 = p1.clone();

        System.out.println("(p1 == p2) = " + (p1 == p2));               // true
        System.out.println("(p == p1) = " + (p == p1));                 // false
        System.out.println("(p1.equals(p2)) = " + (p1.equals(p2)));     // true
        System.out.println("(p1.equals(p)) = " + (p1.equals(p)));
        System.out.println("(p1.equals(new Person())) = " + (p1.equals(new Person())));




        p2.x = 100 ;

        System.out.println("p = " + p.toString());   // same as         System.out.println("p = " + p);
        System.out.println("p1 = " + p1);
        System.out.println("p2 = " + p2);
        System.out.println("p2.hashCode() = " + p2.hashCode());
        System.out.println("Integer.toHexString(p2.hashCode()) = " + Integer.toHexString(p2.hashCode()));




    }
}
