package oop;

import java.awt.*;

public class Point {
    int x , y ,z ;
    Color color ;

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  Point)
        {
            Point t = (Point) obj ;
            return x == t.x && y == t.y && z == t.z ;
        }
        return  false ;
    }

//    @Override
//    public String toString() {
//        return "Point ["+x+", "+y+", "+z+"]";
//    }


    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", HashCode=" + this.hashCode() +
                '}';
    }
}
