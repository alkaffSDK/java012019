package oop;

import java.util.Locale;
import java.util.Random;

public class InterfaceDemo {

    class  A {

    }

    // interface : is a type that contains no instance data fields
    // All variables in the interfaces are public static final
    // By default : methods are public abstract (with no body)
    // interface can have static methods with a body,
    // default method with a body or
    // private method with a body

    interface Flyable{
        //public static  final  int x = 0;

        // public abstract
        String canFly();

        static public  void staticMethod()
        {

        }
        // default here is not an access modifier
        default void defaultMethod()
        {

        }
        private  void privateMethod()
        {

        }
    }
    static class Animal {
        private static final  int x =0  ;
        @Override
        public String toString() {
            return "Animal";
        }
    }

    static class Mammal extends Animal {
        @Override
        public String toString() {
            return "Mammal";
        }
    }

    static class Bird extends Animal {
        @Override
        public String toString() {
            return "Bird";
        }
    }

    static class Penguin extends Bird {
        @Override
        public String toString() {
            return "Penguin";
        }
    }

    static class Falcon extends Bird  implements Flyable{
        @Override
        public String toString() {
            return "Falcon";
        }
//        @Override
        public String canFly() {
            return ", it can fly!";
        }
    }

    static class Bat extends Mammal implements Flyable{
        @Override
        public String toString() {
            return "Bat";
        }

       @Override
        public String canFly() {
            return ", it can fly!";
        }

    }

    static class Cat extends Mammal {
        @Override
        public String toString() {
            return "Cat";
        }
    }

    static class Eagle extends Bird implements Flyable {
        @Override
        public String toString() {
            return "Eagle";
        }
        @Override
        public String canFly() {
            return ", it can fly!";
        }

    }


    public static void main(String[] args) {


         Random random = new Random();
        Animal[] animals = new Animal[20];      // 21 variable and 1 object

        for (int i = 0; i < animals.length; i++) {
            switch (random.nextInt(7)) {
                case 0:
                    animals[i] = new Cat();
                    break;
                case 1:
                    animals[i] = new Bat();
                    break;
                case 2:
                    animals[i] = new Falcon();
                    break;
                case 3:
                    animals[i] = new Penguin();
                    break;
                case 4:
                    animals[i] = new Mammal();
                    break;
                case 5:
                    animals[i] = new Bird();
                    break;
                default:
                    animals[i] = new Eagle();
                    break;
            }

            System.out.printf(Locale.getDefault(), "%3d) %s", i + 1, animals[i].toString());

//            if (animals[i] instanceof Bat  ) {
//                System.out.println(((Bat) animals[i]).canFly());
//            } else if (animals[i] instanceof Falcon) {
//                Falcon f = (Falcon) animals[i];
//                System.out.println(f.canFly());
//            } else
//                System.out.println();

            if(animals[i] instanceof  Flyable)
                System.out.println(((Flyable) animals[i]).canFly());
            else
                System.out.println();
        }

    }


}
