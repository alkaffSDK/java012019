package oop;

import java.util.Locale;
import java.util.Random;

public class AbstractClassDemo {


    // abstract keyword used with
    // 1) methods  : abstract methods have no body (like the methods in the interface )
    // 2) classes  : is the class that may contains an abstract method
    static abstract class ParentAbstract
    {
        public ParentAbstract() {
            System.out.println("ParentAbstract()");
        }
       public abstract  void AbstractMethod();
        public void NonAbstractMethod()
        {
        }
    }

    static public class ChildOfAbstract extends  ParentAbstract {

        public ChildOfAbstract() {
            System.out.println("ChildOfAbstract()");
        }

        @Override
        public void AbstractMethod() {
            System.out.println("ChildOfAbstract:AbstractMethod()");
        }
    }


    static abstract class  AnotherChild extends ParentAbstract {

    }


    static abstract  class  Shape {
        public abstract double area();
    }

    static  class  Triangle extends  Shape {
        private  double Height, Base;

        public Triangle(double height, double base) {
            setHeight(height);
            setBase(base);
        }

        public double getHeight() {
            return Height;
        }

        public void setHeight(double height) {
            Height = height;
        }

        public double getBase() {
            return Base;
        }

        public void setBase(double base) {
            Base = base;
        }

        public double area(){return  0.5* Base * Height ;}

        @Override
        public String toString() {
            return String.format(Locale.getDefault(),"%10s{%7s =%6.2f , %7s =%6.2f }" ,
                    "Triangle","Height",Height,"Base",Base);
        }
    }
    static  class  Rectangle extends  Shape {
        private  double Height, Width;

        public Rectangle(double height, double width) {
            setHeight(height);
            setWidth(width);
        }

        public double getHeight() {
            return Height;
        }

        public void setHeight(double height) {
            Height = height;
        }

        public double getWidth() {
            return Width;
        }

        public void setWidth(double width) {
            Width = width;
        }

        public double area(){return  Width * Height ;}

        @Override
        public String toString() {
            return String.format(Locale.getDefault(),"%10s{%7s =%6.2f , %7s =%6.2f }" ,
                    "Rectangle","Height",Height,"Width",Width);
        }
    }

    public static void main(String[] args) {

//        ParentAbstract anAbstract = new ParentAbstract();
//        anAbstract.AbstractMethod();

        ParentAbstract ps = new ChildOfAbstract();
        //ps.AbstractMethod();

        System.out.println("--------------------");
        ParentAbstract ps1 = new ParentAbstract() {

            @Override
            public void AbstractMethod() {
                System.out.println("AnonymousInnerType:AbstractMethod()");
            }
            public void method()
            {

            }
        };
        System.out.println("--------------------");
        ps.AbstractMethod();            // ChildOfAbstract:AbstractMethod()
        ps1.AbstractMethod();           // AnonymousInnerType:AbstractMethod()


        Random r = new Random();
        Shape[] shapes = new Shape[10];
        for(int i=0;i<shapes.length;i++)
        {
            switch (r.nextInt(2))
            {
                case  0 :
                    shapes[i] = new Rectangle(r.nextInt(10),r.nextInt(10)); break;
                case  1 :
                    shapes[i] = new Triangle(r.nextInt(10),r.nextInt(10)); break;
            }

            System.out.printf(Locale.getDefault(),"The area for %30s is %6.2f%n",shapes[i], shapes[i].area());
        }
    }
}
