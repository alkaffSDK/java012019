package exceptions;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ExceptionDemo {

    /**
     * This method divide 2 integer numbers
     * @param a the first number
     * @param b the second number
     * @return
     * @throws ArithmeticException if the second number is equal to zero.
     */
    public static Integer divide(int a, int b) throws ArithmeticException {
//        if(b == 0) {
//            System.err.println("Error");
//            return  null ;
//        }
        return a / b;                    // 1
    }

    public static void main(String[] args) {
        //  main(args);
        Scanner scanner = new Scanner(System.in);
        int x = 0, y = 0;

        do {
            try {
                System.out.print("X:");
                x = scanner.nextInt();
                System.out.print("Y:");
                y = scanner.nextInt();
                scanner.nextLine();
//            if (y != 0)
                System.out.printf(Locale.getDefault(), "%d / %d = %s%n", x, y, divide(x, y));   // 2
//            else
//                System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
                System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);   // 2
            } catch (InputMismatchException ex) {

            } catch (IllegalStateException | NoSuchElementException ex) {
                throw  new MyException("this is my exception");
                // multi catch
            } catch (Exception e) {
            } catch (Throwable t) {
                if (t instanceof Error) {
                    // Handle errors
                } else if (t instanceof Exception) {
                    if (t instanceof RuntimeException) {
                        if (t instanceof ArithmeticException || t instanceof  NullPointerException) {

                        }
                    } else if (t instanceof IOException) {

                    }
                }
            }
            System.out.println("Press enter to continue or Q to exit");
        } while (!scanner.nextLine().equalsIgnoreCase("q"));

        System.out.println("Thank you");
        scanner.close();


    }
}
