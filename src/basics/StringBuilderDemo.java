package basics;

public class StringBuilderDemo {

    public static void main(String[] args) {
        // Since the string is immutable data type and it is not changeable, how to build a string
        // How to save an array inside a string 
        
        int[] array =  {5,8,2,1,4};
        // ths first option 
        
        String str = "{";
        for(int i=0;i<array.length;i++)
        {
            str += array[i] + (i != array.length-1 ? ", ":"}");
        }
        System.out.println("str = " + str);
        // this option (the first option) is bad since it will create many string values
        // for the above example the string created are
        // "{"
        // "{5, "
        // "{5, 8, "
        // "{5, 8, 2, "
        // "{5, 8, 2, 1, "
        // "{5, 8, 2, 1, 4}"


        // better solution is to use StringBuilder or StringBuffer

        StringBuilder stringBuilder = new StringBuilder("{");
        for(int i=0;i<array.length;i++)
        {
            stringBuilder.append(array[i]).append(i != array.length-1 ? ", ":"}") ;
        }
        str = stringBuilder.toString() ;
        System.out.println("str = " + str);


//        "a".substring(0,3);

    }
}
