package basics;


public class MethodsDemo {
    // Method: named block of statements that has a (output) return type and optional
    // inputs (parameter list)
    // All methods (in java) must be written directly inside a class
    // Syntax : 1) definition
            // [specifier] [modifier] <return_data_type> <method_name>([parameter list]) { [body]}
    //      2) call method      [<variable_name>/<class_name>.]method_name([argument_list]);
    //


    {
        // this is a block
    }

    void method (){

    }
   static  class MyClass {
        int a ;
        static  int m ;
        void method()
        {
            System.out.println("This is a non method which is exist in each object of the class ");
            return ;        // return will end the execution of the method
            //System.out.println();
        }
        static  void staticMethod()
        {
            System.out.println("This is a static method which is exist in the class itself");
        }

        int method1()
        {
            return  10 ;
        }

        double method(int a, double b)
        {
            return  a + b ;
        }

       public static void main(String[] args) {
           staticMethod();
           MyClass aClass = new MyClass();
           aClass.method();
           double d = 15.325 ;

           System.out.println(aClass.method(10,d));             // 25.325
       }
   }

    public static void main(String[] args) {

        MyClass obj = new MyClass();
        obj.a = 10 ;
        obj.method();
        // method();

        MyClass.staticMethod();

        MyClass.m = 20 ;

    }

}
