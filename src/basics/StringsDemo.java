package basics;

public class StringsDemo {
    public static void main(String[] args) {
        // Strings : array of characters

        // primitive (value type)
        int a  = 10 ;
        // non-primitive (reference type)
        Object o = new Object();

        String str = "this is a string";
        String str1 = "this is a string";
        
        String string = new String("this is a string");
        String string1 = new String("This is a string");


        System.out.println("str = " + str);
        System.out.println("string = " + string);
        System.out.println("---------------------");
        System.out.println("(str == str1) = " + (str == str1));                 // true
        System.out.println("(string == string1) = " + (string == string1));     // false
        System.out.println("(str == string) = " + (str == string));             // false

        System.out.println("---------------------");
        System.out.println("(str.equals(str1)) = " + (str.equals(str1)));
        System.out.println("(str.equals(string)) = " + (str.equals(string)));
        System.out.println("(string.equals(string1)) = " + (string.equals(string1)));

        System.out.println("---------------------");
        System.out.println("(str.equals(str1)) = " + (str.equalsIgnoreCase(str1)));
        System.out.println("(str.equals(string)) = " + (str.equalsIgnoreCase(string)));
        System.out.println("(string.equals(string1)) = " + (string.equalsIgnoreCase(string1)));

        str = "   new      String    ";

        System.out.println("str.strip() = " + str.strip());
        System.out.println("str.trim()  = " + str.trim());




    }
}
