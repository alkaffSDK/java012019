package basics;

public class ConsoleOutput {

    public static void main(String[] args) {
        System.out.println("ConsoleOutputAndInput");
        System.out.print("just a print");

        System.out.println(10);
        System.out.println(1 + 2 + 3);          // 6
        System.out.println("1" + 2 + 3);        // 123
        System.out.println("1" + (2 + 3));      // 15
        System.out.println(1 + "2" + 3);        // 123
        System.out.println(1 + 2 + "3");        // 33
        System.out.println(1 + 2 + '3');        // 54 (ASCII code of '3' is 51)


    }
}
