package basics;

import java.util.Locale;
import java.util.Scanner;

public class ConsoleInput {
    public static void main(String[] args) {

        // create scanner object
        Scanner scanner = new Scanner(System.in);
        // use the default localization settings
        scanner.useLocale(Locale.getDefault());

        // use the scanner object to input from console

        System.out.print("Please enter a value for A:");
        int a = scanner.nextInt() ;
        System.out.println("a = " + a);


        System.out.print("Please enter a value for D:");
        double d = scanner.nextDouble() ;
        System.out.println("d = " + d);
        // To remove the next line after the double
        scanner.nextLine();

        System.out.print("Please enter a full line:");
        String input = scanner.nextLine();

        System.out.println("input = " + input);

        System.out.print("Please enter a full line:");
       input = scanner.next();

        System.out.println("input = " + input);



        // don't forget to close the scanner !
        scanner.close();

    }
}
