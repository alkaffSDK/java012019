package shared;

public class Person {

    public int Id;
    public  String Name ;
    public Gender gender ;

    public void DoSome()
    {
        System.out.println("Person");
    }
    public  void MethodInPerson()
    {

    }

    @Override
    public String toString() {
        return "Person{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", gender=" + gender +
                '}';
    }
}
