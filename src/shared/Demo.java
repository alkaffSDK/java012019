package shared;

import java.util.Objects;

public class Demo {

    private int ID ;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Demo demo = (Demo) o;
        return ID == demo.ID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }
}
