package shared;

public class User {
    private static final int MAX_AGE =  150;

    private int id ;
    private int age ;
    private String Name;
    private String Login;
    private String Password;

    public void setId(int id)
    {
        if(id >0)
            id = id ;
        else
            id = 0 ;
    }
    public int getId(){return  id;}

    public int getAge(){return  age ;}

    /**
     * To set the age of the user.
     * @param age   non-negative integer value for the number of years, less than 150
     * @throws IllegalArgumentException : if the age value less than 0 or more than 150
     */
    public void setAge(int age) throws IllegalArgumentException
    {
        if(age >0 && age < MAX_AGE)
            this.age = age ;
        else
            throw new IllegalArgumentException("Invalid age value");
    }

    public boolean changePassword(String newPass)
    {
        if(newPass ==null || newPass.length() <6 || newPass.equals(Password))
            return  false ;
        else
        {
            Password = newPass;
            return  true;
        }
    }

    public boolean checkPassword(String password)
    {
        return  password.equals(this.Password);
    }

    public String getName() {
        return Name;
    }

    public boolean setName(String name) {
        for(int i=0;i<name.length();i++)
        {
            if(!Character.isLetter(name.charAt(i)))
                return false;
        }

        Name = name;
        return  true ;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", age=" + age +
                ", Name='" + Name + '\'' +
                ", Login='" + Login + '\'' +
                ", Password='" + Password + '\'' +
                '}';
    }
}
