package shared;

public class EncapsulationClass {

    public  int Public ;        // accessible from anywhere
    protected  int Protected ;  // accessible from inside the class package any any child object from any package
    int Package ;       // Default, accessible from inside the class package only
    private  int Private ;          // accessible from inside the class only

    public  void method()
    {
        Private = 10 ;
    }
}
